import annotation.TestAnnotation;
import realisation.AppAnn;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;


public class Manager {


    public static void main(String[] args) throws Exception {
        Class clazz = AppAnn.class;
        System.out.println();
        System.out.println();
        annFields(clazz);
        annMethod(clazz);

        AppAnn app = new AppAnn(5, "test");
        reflObject(app);
    }

    private static void annMethod(Class clazz) {
        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            if (method.isAnnotationPresent(TestAnnotation.class)) {
                TestAnnotation ann = (TestAnnotation) method.getAnnotation(TestAnnotation.class);
                String name = ann.value();
                int i = ann.i();
                System.out.print("method: " + method.getName());
                System.out.println(" Annotation = " + name + " i= " + i);
            }
        }
    }

    private static void annFields(Class clazz) {
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(TestAnnotation.class)) {
                TestAnnotation ann = (TestAnnotation) field.getAnnotation(TestAnnotation.class);
                String name = ann.value();
                int i = ann.i();
                System.out.print("field: " + field.getName());
                System.out.println(" Annotation = " + name + " i= " + i);
            }
        }
    }

    public static void reflObject(Object smth) throws Exception {
        Class clazz = smth.getClass();
        System.out.println("Class is " + clazz.getSimpleName());
        Constructor constructor = clazz.getDeclaredConstructor();
        System.out.println("The name of constructor is " + constructor.getName());
        System.out.println("methods of class are : ");
        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            System.out.println("  " + method.getName());
        }
        System.out.println("fields of class are : ");
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            System.out.println("  " + field.getName());
        }

    }
}

