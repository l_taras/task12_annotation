package realisation;

import annotation.TestAnnotation;

public class AppAnn {
    @TestAnnotation(i = 1)
    private int i;
    @TestAnnotation(value = "name")
    private String name;

    public AppAnn() {

    }
    public AppAnn(int i, String name) {
        this.i = i;
        this.name = name;
    }

    public int getI() {
        return i;
    }

    public String getName() {
        return name;
    }

    @TestAnnotation(value = "Method", i = 0)
    public void method() {
        System.out.println("method");
    }

    @TestAnnotation(i = 0)
    public void method1() {
        System.out.println("method1");
    }

    @TestAnnotation(value = "Method2")
    public void method2() {
        System.out.println("method2");
    }


}
