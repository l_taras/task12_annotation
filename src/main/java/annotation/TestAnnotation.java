package annotation;


import java.lang.annotation.*;

@Inherited
@Target({ElementType.FIELD, ElementType.CONSTRUCTOR, ElementType.METHOD})
@Retention(value = RetentionPolicy.RUNTIME)
public @interface TestAnnotation {
    enum Status {FIXED, UNTRACKED, CHECKED}

    String value() default "smth";

    int i() default 1;
}
